package rmi;

import java.rmi.Remote; 
import java.rmi.RemoteException; 

public interface Interface extends Remote {
	public void save(Video video) throws RemoteException;
	public Video get(String id) throws RemoteException;
}
