package rmi;

import java.io.Serializable;

public class Video implements Serializable {

	private static final long serialVersionUID = 1L;

	private byte[] data;
	private String id;

	public Video(){}

	public Video(String id, byte[] data){
		setId(id);
		setData(data);
	}

	public byte[] getData(){
		return data;
	}

	public void setData(byte[] data){
		this.data = data;
	}

	public String getId(){
		return this.id;
	}

	public void setId(String id){
		this.id = id;
	}

}
