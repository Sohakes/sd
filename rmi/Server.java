import java.rmi.registry.Registry; 
import java.rmi.registry.LocateRegistry; 
import java.rmi.RemoteException; 
import java.rmi.server.UnicastRemoteObject; 

import rmi.*;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;

public class Server implements Interface {
	private Registry registry;
	private MongoClient mongoClient;
	private DBCollection videos;
	private DB db;

	public Server(){
		try {
			mongoClient = new MongoClient("192.168.56.1", 27017);
		} catch(Exception e){
			System.out.println("Error when connecting");
		}

		db = mongoClient.getDB("sd");
		// boolean auth = db.authenticate("videouser", "videopassword".toCharArray());
		// if(!auth) System.out.println("Wrong username/pass");
		videos = db.getCollection("video");
	}

	public void save(Video video) throws RemoteException {
	System.out.println("chegou");
		BasicDBObject v = new BasicDBObject();
		v.put("id", video.getId());
		v.put("data", video.getData());

		videos.insert(v);

		System.out.println("Salvo: " + video.getId());
	}

	public Video get(String id) throws RemoteException {
		System.out.println("chegou");
		BasicDBObject query = new BasicDBObject();
		query.put("id", id);

		DBCursor cursor = videos.find(query);
		Video video = null;


		if(cursor.hasNext()){
			BasicDBObject v = (BasicDBObject) cursor.next();
			
			byte[] data = (byte[]) v.get("data");

			video = new Video(id, data);

			System.out.println("Retornado: " + id);

			return video;
		} else
			System.out.println("Erro ao retornar: " + id);

		return video;
	}

	public static void main(String args[]) { 
		// default registry port of 1099.
		try { 
		
			Server obj = new Server(); 
			Interface stub = (Interface) UnicastRemoteObject.exportObject(obj, 0); 
			Registry registry = LocateRegistry.getRegistry(1099);
			registry.bind("RMIserver", stub); 
			System.err.println("Servidor RMI pronto"); 
		} catch (Exception e) { 
			System.err.println("Error: " + e.toString()); 
			e.printStackTrace(); 
		}
	}
} 
