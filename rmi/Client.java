// Este cliente é apenas para testes, ele representa chamadas vindas do WS

import java.rmi.registry.LocateRegistry; 
import java.rmi.registry.Registry; 
import java.util.Scanner;
import rmi.*;

public class Client { 

	public static void main(String[] args) { 

		// if (System.getSecurityManager() == null) {
		// 	System.setSecurityManager(new SecurityManager());
		// }

		try { 
			Registry registry = LocateRegistry.getRegistry(); 
			Interface server = (Interface) registry.lookup("RMIserver");
			Scanner scanner = new Scanner(System.in);
			int i = 1;

			while(i != 0){
				System.out.println("0: sair, 1: novo video, 2: pegar video");
				System.out.print("Escolha: ");

				i = scanner.nextInt();

				System.out.println();

				if(i == 1){
					String id, data;

					System.out.print("Id: ");
					id = scanner.next();

					System.out.print("Data: ");
					data = scanner.next();

					Video v = new Video(id, data.getBytes());
					server.save(v);

				} else if(i == 2){
					String id;

					System.out.print("Id: ");
					id = scanner.next();

					Video v = server.get(id);

					if(v != null)
						System.out.println(v.getId() + " : " + new String(v.getData(), "UTF-8"));
					else
						System.out.println("Erro ao pegar video");
				}
			}

		} catch (Exception e) { 
			System.err.println("Erro: " + e.toString()); 
			e.printStackTrace(); 
		} 

	} 
} 
