package com.mytube;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends Activity {
	EditText descText;
	TextView descVideoText;
	EditText filenameText;
	EditText videoidText;
	Button receiveButton;
	Button sendButton;
	Button choseButton, voltar;
	VideoView videoView;
	File f;
	byte[] fileData;
	Activity _this = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		_this = this;

		setContentView(R.layout.main);
		descText = (EditText) findViewById(R.id.descricao);
		filenameText = (EditText) findViewById(R.id.filepath);
		videoidText = (EditText) findViewById(R.id.videoid);
		receiveButton = (Button) findViewById(R.id.receive);
		sendButton = (Button) findViewById(R.id.send);
		choseButton = (Button) findViewById(R.id.chosefile);


		videoView = (VideoView) findViewById(R.id.videoView1);



		receiveButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String WS_NAMESPACE = "http://videos/";
				String URL = "http://" + filenameText.getText().toString() + ":8084/VideoWs/ManageVideos?wsdl";
				String DOWNLOAD_OPERATION = "downloadVideo";
				String SOAP_DOWNLOAD_OPERATION = "\"http://videos/downloadVideo\"";
				SoapObject request = new SoapObject(WS_NAMESPACE, DOWNLOAD_OPERATION);

				request.addProperty("id", videoidText.getText().toString());

				//Envelope SOAP
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				new MarshalBase64().register(envelope);
				envelope.encodingStyle = SoapEnvelope.ENC;
				envelope.setAddAdornments(false);
				envelope.implicitTypes = true;
				envelope.dotNet = false;
				envelope.setOutputSoapObject(request);

				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.debug=true;
				try {
					
					//Chamada ao WS
					androidHttpTransport.call(SOAP_DOWNLOAD_OPERATION, envelope);
					
					//String com o retorno
					SoapObject response = (SoapObject) envelope.bodyIn;

					if(response.getPropertyCount() < 2){
						Utilities.alert(_this, "MyTube - Id error", "Error", "Id does not exist");
						return;
					}

					String newpath = Environment.getExternalStorageDirectory()+"/arquivo";
					FileUtils.writeByteArrayToFile(new File(newpath), 
						Base64.decode(response.getProperty(0).toString(), Base64.DEFAULT) );

					descText.setText(response.getProperty(1).toString());

					videoView.setVideoPath(newpath);

					Utilities.alert(_this, "MyTube - Download", "Success", "Video was downloaded");

					videoView.start();  
				} catch (Exception e) {
					Log.i("MyActivity", e.toString());
					Utilities.alert(_this, "MyTube - Download error", "Error", "Error downloading");
				}
			}
		});

		choseButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showFileChooser();              
			}
		});

		sendButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				//Constantes com os dados do WS
				String WS_NAMESPACE = "http://videos/";
				String URL = "http://" + filenameText.getText().toString() + ":8084/VideoWs/ManageVideos?wsdl";
				String UPLOAD_OPERATION = "uploadVideo";
				String SOAP_UPLOAD_OPERATION = "\"http://videos/uploadVideo\"";
				
				//Criando os padâmetros de entrada
				SoapObject request = new SoapObject(WS_NAMESPACE, UPLOAD_OPERATION);

				request.addProperty("desc", descText.getText().toString());
				request.addProperty("data", (fileData));

				//Envelope SOAP
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				new MarshalBase64().register(envelope);
				envelope.encodingStyle = SoapEnvelope.ENC;
				envelope.setAddAdornments(false);
				envelope.implicitTypes = true;
				envelope.dotNet = false;
				envelope.setOutputSoapObject(request);

				HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
				androidHttpTransport.debug=true;
				try {
					//Chamada ao WS
					androidHttpTransport.call(SOAP_UPLOAD_OPERATION, envelope);
					
					//String com o retorno
					SoapObject response = (SoapObject) envelope.bodyIn;

					Log.i("res count", response.getProperty(0).toString());
								
					String strid = (String) response.getProperty(0).toString();
					Log.i("res string",strid);
					videoidText.setText(strid);

					Utilities.alert(_this, "MyTube - Upload success", "Success", "Uploading was successful");			
				} catch (Exception e) {
					Log.i("MyActivity", e.toString());
					Utilities.alert(_this, "MyTube - Upload error", "Error", "Error uploading");
				}

			}
		});

	}

	private static final int FILE_SELECT_CODE = 0;

	private void showFileChooser() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
		intent.setType("*/*"); 
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		try {
			startActivityForResult(
				Intent.createChooser(intent, "Select a File to Upload"),
				FILE_SELECT_CODE);
		} catch (android.content.ActivityNotFoundException ex) {
			// Potentially direct the user to the Market with a Dialog
			Toast.makeText(this, "Please install a File Manager.", 
				Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case FILE_SELECT_CODE:
			if (resultCode == RESULT_OK) {
				Uri uri = data.getData();
				try {
					String path = getPath(this, uri);
					f = new File(path);
					fileData = FileUtils.readFileToByteArray(f);
				} catch (URISyntaxException ex) {
					Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public static String getPath(Context context, Uri uri) throws URISyntaxException {
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = { "_data" };
			Cursor cursor = null;

			try {
				cursor = context.getContentResolver().query(uri, projection, null, null, null);
				int column_index = cursor.getColumnIndexOrThrow("_data");
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (Exception e) {
			}
		}
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	} 

}
