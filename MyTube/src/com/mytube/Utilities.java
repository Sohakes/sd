package com.mytube;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class Utilities {
	
	@SuppressWarnings("deprecation")
	public static void alert(Activity view, String statusBarMsg, String title, String details){
		int requestCode = 1, notificationID = 2;
		
		NotificationManager manager = (NotificationManager) view.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = new Notification(com.mytube.R.drawable.ic_launcher, statusBarMsg, System.currentTimeMillis());

		PendingIntent contentIntent = PendingIntent.getActivity(view, requestCode, new Intent(view, MainActivity.class), 0);
		
		notification.setLatestEventInfo(view, title, details, contentIntent);
		
		manager.notify(notificationID, notification);
	}
}