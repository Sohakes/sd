#!/usr/bin/env python
import sys
from omniORB import CORBA, PortableServer
import CosNaming, Video__POA
import pymongo, random, string
from pymongo import Connection, MongoClient
connection = MongoClient("192.168.56.1", 27017)
db = connection["sd"]
# db.authenticate("videouser", "videopassword")
#mongodb://videouser:videopassword@ds053708.mongolab.com:53708/videos
#Echo = Idgen
#Example = Video
#echoString = generateId
# Define an implementation of the Idgen interface
class Idgen_i (Video__POA.Idgen):
	def generateId(self):
		videos = db.video
		nid = str(''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(5))) 
		while (videos.find({"id": nid})).count() != 0: #para a minima possibilidade de nao ser unico
			nid = str(''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(5))) 
		print "generateId() called with:", nid
		return nid

	def checkId(self, nid):
		ret = db.video.find({"id": nid}).count() != 0
		print "checkId() called with: ", nid, " returning ", ret
		return ret
   

# Initialise the ORB and find the root POA
orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
poa = orb.resolve_initial_references("RootPOA")

# Create an instance of Idgen_i and an Idgen object reference
ei = Idgen_i()
eo = ei._this()

# Obtain a reference to the root naming context
obj         = orb.resolve_initial_references("NameService")
rootContext = obj._narrow(CosNaming.NamingContext)

if rootContext is None:
	print "Failed to narrow the root naming context"
	sys.exit(1)

# Bind a context named "test.my_context" to the root context
# name = [CosNaming.NameComponent("test", "my_context")]
# try:
# 	testContext = rootContext.bind_new_context(name)
# 	print "New test context bound"
	
# except CosNaming.NamingContext.AlreadyBound, ex:
# 	print "Test context already exists"
# 	obj = rootContext.resolve(name)
# 	testContext = obj._narrow(CosNaming.NamingContext)
# 	if testContext is None:
# 		print "test.mycontext exists but is not a NamingContext"
# 		sys.exit(1)

# Bind the Idgen object to the test context
name = [CosNaming.NameComponent("VideoIdgen", "Object")]
try:
	rootContext.bind(name, eo)
	print "New VideoIdgen object bound"

except CosNaming.NamingContext.AlreadyBound:
	rootContext.rebind(name, eo)
	print "VideoIdgen binding already existed -- rebound"

# Activate the POA
poaManager = poa._get_the_POAManager()
poaManager.activate()

# Block for ever (or until the ORB is shut down)
orb.run()
