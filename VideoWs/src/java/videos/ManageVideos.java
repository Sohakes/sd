package videos;

import corba.Idgen;
import corba.IdgenHelper;
import Common.Remote;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import appengine.*;
import rmi.*;
import java.io.UnsupportedEncodingException;
import org.apache.catalina.util.Base64;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;


@WebService(serviceName = "ManageVideos")
public class ManageVideos {
    
    
    static Idgen idgenImpl;
    
    public static void main(String[] args) throws UnsupportedEncodingException{
    }
    
    public static boolean sendVideoDesc(String id, String desc){
        ManageAppEngine app = new ManageAppEngine();
        return app.send(id,desc);

    }
    
    public static String receiveVideoDesc(String id){
        ManageAppEngine app = new ManageAppEngine();
        return app.receive(id);
    }
    
    public static boolean checkId(String id) {
        try{
            
            // cria e inicia o ORB
            String [] args = {"-ORBInitRef", "NameService=corbaname::" + Remote.VM + ":" + Remote.CORBA_PORT};
            ORB orb = ORB.init(args, null);

            org.omg.CORBA.Object objRef =
                orb.resolve_initial_references("NameService");
            
            // Usando NamingContextExt: servico de nomes interoperavel
            
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            idgenImpl = IdgenHelper.narrow(ncRef.resolve_str("VideoIdgen.Object"));

            boolean nid = idgenImpl.checkId(id);
            System.out.println("ManageVideos.checkId: " + id + " is " + nid);
            return nid;

        } catch (InvalidName e) {
          System.out.println("InvalidName on ManageVideos.checkId: " + e) ;
          e.printStackTrace(System.out);
        } catch (CannotProceed e) {
            System.out.println("CannotProceed on ManageVideos.checkId: " + e) ;
            e.printStackTrace(System.out);
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
            System.out.println("org.omg.CosNaming.NamingContextPackage.InvalidName on ManageVideos.checkId: " + e) ;
            e.printStackTrace(System.out);
        } catch (NotFound e) {
            System.out.println("NotFound on ManageVideos.checkId: " + e) ;
            e.printStackTrace(System.out);
        }
        
        return false;
    }
    
    public static String getId() {
        try{
            
            // cria e inicia o ORB
            String [] args = {"-ORBInitRef", "NameService=corbaname::" + Remote.VM + ":" + Remote.CORBA_PORT};
            ORB orb = ORB.init(args, null);

            org.omg.CORBA.Object objRef =
                orb.resolve_initial_references("NameService");
            
            // Usando NamingContextExt: servico de nomes interoperavel
            
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            idgenImpl = IdgenHelper.narrow(ncRef.resolve_str("VideoIdgen.Object"));

            System.out.println("Obtained a handle on server object: ");
            String nid = idgenImpl.generateId();
            System.out.println("ManageVideos.getId: " + nid);
            return nid;

        } catch (InvalidName e) {
          System.out.println("InvalidName on ManageVideos.getId: " + e) ;
          e.printStackTrace(System.out);
        } catch (CannotProceed e) {
            System.out.println("CannotProceed on ManageVideos.getId: " + e) ;
            e.printStackTrace(System.out);
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
            System.out.println("org.omg.CosNaming.NamingContextPackage.InvalidName on ManageVideos.getId: " + e) ;
            e.printStackTrace(System.out);
        } catch (NotFound e) {
            System.out.println("NotFound on ManageVideos.getId: " + e) ;
            e.printStackTrace(System.out);
        }
        return null;
    }

    /**
     * Web service operation
     * @param id id do video
     * @return VideoWS que representa o video (classe)
     */
    @WebMethod(operationName = "downloadVideo")
    public String[] downloadVideo(@WebParam(name = "id") String id) {
        if(!checkId(id)){
            System.out.println("Id '" + id + "' does not exist at ManageVideos.downloadVideo");
            String[] error = new String[]{""};
            return error;
        }
        
        VideoWs v = new VideoWs();
        Client client = new Client();
        v.setId(id);
        v.setData(client.downloadVideo(id));
        v.setDesc(receiveVideoDesc(id));
        
        System.out.println("ManageVideos.downloadVideo: " + id);
        String x = Base64.encode(v.getData());
        String y = v.getDesc();
        String[] arr = new String[]{x,y};
        return arr;
    }

    /**
     * Web service operation
     * @param desc Descricao do video
     * @param data Dados do video
     * @return id gerada pelo CORBA
     */
    @WebMethod(operationName = "uploadVideo")
    public String uploadVideo(@WebParam(name = "desc") String desc, @WebParam(name = "data") byte[] data) {
        String id = getId();
        sendVideoDesc(id, desc);
        Client client = new Client();
        client.uploadVideo(id, data);
        System.out.println("ManageVideos.uploadVideo: " + id);
        return id;
    }
    
}



