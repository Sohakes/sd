package appengine;

import Common.Remote;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class ManageAppEngine {
    public boolean send(String id, String desc){
        try {
                URL url = new URL(Remote.APPENGINE + "newvideo");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                OutputStreamWriter writer = new
                                OutputStreamWriter(connection.getOutputStream());
                writer.write("videoid=" + id + "&desc=" + desc);
                writer.close();
                return connection.getResponseCode() == HttpURLConnection.HTTP_OK;
        } catch (MalformedURLException e) {
                System.out.println("MalformedURLException on ManageAppEngine.send");
        } catch (IOException e) {
                System.out.println("IOException on ManageAppEngine.send");
        }
        return false;
    }
    
    public String receive(String id)
    {
            String desc = null;
                try {
                        URL url = new URL(Remote.APPENGINE + "getvideo?videoid="+id);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.setDoOutput(true);
                        connection.setRequestMethod("GET");
                        desc = connection.getHeaderField("desc");
                        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                            return null;
                } catch (MalformedURLException e) {
                    System.out.println("MalformedURLException on ManageAppEngine.receive");
                } catch (IOException e) {
                    System.out.println("IOException on ManageAppEngine.receive");
                }
                return desc;
        }
    
}
