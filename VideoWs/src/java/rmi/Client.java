package rmi;

import Common.Remote;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry; 
import java.rmi.registry.Registry; 
import java.util.Scanner;

public class Client { 
    
    public boolean uploadVideo(String id, byte[] data){
        try {
            Registry registry = LocateRegistry.getRegistry(Remote.VM, Remote.RMI_PORT);
            Interface server = (Interface) registry.lookup("RMIserver");
            Video v = new Video(id, data);
            System.out.println("Client.uploadVideo de: " + id);
            server.save(v);     
            return true;

        } catch (NotBoundException e) {
            System.err.println("NotBoundException on Client.uploadVideo: " + e.toString());
            e.printStackTrace();
            
        } catch (RemoteException e) {
            System.err.println("RemoteException on Client.uploadVideo: " + e.toString());
            e.printStackTrace();
        }
        return false;
    }
    
    public byte[] downloadVideo(String id){
        try {
            Registry registry = LocateRegistry.getRegistry(Remote.VM, Remote.RMI_PORT);
            Interface server = (Interface) registry.lookup("RMIserver");
            int i = 1;
            Video v = server.get(id);
            
            if (v != null) {
                System.out.println("Client.downloadVideo: " + v.getId());
                return v.getData();
            } else {
                System.out.println("There was a problem downloading the file");
            }

        } catch (NotBoundException e) {
            System.err.println("NotBoundException on Client.downloadVideo: " + e.toString());
            e.printStackTrace();
        } catch (RemoteException e) {
            System.err.println("RemoteException on Client.downloadVideo: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) { 
        try {
            Registry registry = LocateRegistry.getRegistry();
            Interface server = (Interface) registry.lookup("RMIserver");
            Scanner scanner = new Scanner(System.in);
            int i = 1;

            while (i != 0) {
                System.out.println("0: sair, 1: novo video, 2: pegar video");
                System.out.print("Escolha: ");

                i = scanner.nextInt();

                System.out.println();

                if (i == 1) {
                    String id, data;

                    System.out.print("Id: ");
                    id = scanner.next();

                    System.out.print("Data: ");
                    data = scanner.next();

                    Video v = new Video(id, data.getBytes());
                    server.save(v);

                } else if (i == 2) {
                    String id;

                    System.out.print("Id: ");
                    id = scanner.next();

                    Video v = server.get(id);

                    if (v != null) {
                        System.out.println(v.getId() + " : " + new String(v.getData(), "UTF-8"));
                    } else {
                        System.out.println("Erro ao pegar video");
                    }
                }
            }

        } catch (Exception e) {
            System.err.println("Erro: " + e.toString());
            e.printStackTrace();
        }

    } 
        
        
} 
